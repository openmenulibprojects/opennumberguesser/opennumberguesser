package it.lundstedt.erik.randomNumGen;

import it.lundstedt.erik.settings.*;
import java.math.*;

public class RNDNumGen {
  /*settings*/
  RNDSettings settings;
  int times = 1;
  // int num = (int) (Math.random()*10*times+1);
  int num;

  public RNDNumGen(RNDSettings settings) {
    this.settings = settings;
    num = (int) (Math.random() * settings.getMax() + 1);
    // System.out.println("maxi:" + settings.getMax());
    // num=10;
  }

  public int getRND() {
    return num;
  }

  public int guess(int guessed) {
    if (guessed == num) {
      return settings.getCorrect();
    } else if (guessed >= num) {
      // System.out.println(guessed + " more?");
      return settings.getHigh();
    } else if (guessed <= num) {
      // System.out.println(guessed + " less?");
      return settings.getLow();
    } else {
      return settings.getErr();
    }
  }
}
