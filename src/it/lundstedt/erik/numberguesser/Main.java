package it.lundstedt.erik.numberguesser;

import it.lundstedt.erik.menu.*;
import it.lundstedt.erik.randomNumGen.RNDNumGen;
import it.lundstedt.erik.settings.*;

public class Main {

  public static int maxLVL;

  public static void main(String[] args) {
    MenuSettings menuCFG = new MenuSettings();
    // menuCFG.setShowDebug(true);
    Menu.licence("numberguesser", "0.0.0.2");
    RNDSettings settings = new RNDSettings();
    settings.setMax(100);
    // RNDNumGen rnd=new RNDNumGen(settings);
    /*
    System.out.println(rnd.guess(10));
    System.out.println(rnd.guess(5));
    System.out.println(rnd.guess(15));
    */
    /*        int rand=0,max=15;

            for (int i = 0; i <=20; i++) {
                rand= (int) (Math.random()*max+1);

            if (rand>=11) {
                Menu.debug(String.valueOf(rand),true);
            }else
            {
                System.out.println(rand);
            }
            }
    */
    maxLVL = 3;
    RNDSettings[] cfgArr = new RNDSettings[maxLVL];
    cfgArr[0] = new RNDSettings();
    cfgArr[0].setLevel(0);
    cfgArr[0].setMax(10);
    cfgArr[1] = new RNDSettings();
    cfgArr[1].setLevel(1);
    cfgArr[1].setMax(20);
    cfgArr[2] = new RNDSettings();
    cfgArr[2].setLevel(2);
    cfgArr[2].setMax(30);
    for (int i = 0; i <= 2; i++) {

      guess(new RNDNumGen(cfgArr[i]), cfgArr[i], menuCFG);
    }
  }

  public static void guess(RNDNumGen rnd, RNDSettings settings, MenuSettings menuCFG) {
    MenuSettings resultMenuCFG = new MenuSettings();
    resultMenuCFG.setShowDebug(menuCFG.isShowDebug());
    // =menuCFG;
    boolean loop = true;
    Menu mainMenu = new Menu();
    Menu resultMenu = new Menu();
    int guessed;
    String[] header =
        new String[] {
          "max answer value:" + String.valueOf(settings.getMax()),
          "level:",
          String.valueOf(settings.getLevel()),
          "RESULT:"
        };

    Menu.drawItems(
        new String[] {
          "you are currently on level:" + String.valueOf(settings.getLevel()),
          "guess what number i'm \"thinking\" of "
        });

    String[] menuItems = new String[] {"", "", "", ""};
    String debug = " ";
    while (loop) {
      System.out.print(">");
      guessed = resultMenu.getChoice();
      int check = rnd.guess(guessed);

      if (check == settings.getCorrect()) {
        // ("correct");
        menuItems[0] = "correct";
        menuItems[1] = "you guessed:";
        menuItems[2] = String.valueOf(guessed);
        debug = String.valueOf(rnd.getRND());
        resultMenuCFG.setShowDebug(false);
        loop = false;
        if (settings.getLevel() <= maxLVL-1) {
          menuItems[3] = "on to the next level";
        } else if (settings.getLevel() == maxLVL-1) {
          menuItems[3] = "congratulations, you have won";
        }

      } else if (check == settings.getHigh()) {
        menuItems[0] = "TOO HIGH";
        menuItems[1] = "you guessed:";
        menuItems[2] = String.valueOf(guessed);
        menuItems[3] = "guess again";
        debug = String.valueOf(rnd.getRND());
      } else if (check == settings.getLow()) {
        menuItems[0] = "TOO LOW";
        menuItems[1] = "you guessed:";
        menuItems[2] = String.valueOf(guessed);
        menuItems[3] = "guess again";
        debug = String.valueOf(rnd.getRND());
      } else {

      }
      resultMenu.setHeader(header);
      resultMenu.setMenuItems(menuItems);
      resultMenu.drawMenu();
      Menu.debug(debug, resultMenuCFG.isShowDebug());
      // resultMenuCFG.setShowDebug(true);
      if (resultMenuCFG.isShowDebug()) {
        Menu.drawItems(
            new String[] {
              "menuCFG",
              String.valueOf(menuCFG.isShowDebug()),
              "resultMenuCFG",
              String.valueOf(resultMenuCFG.isShowDebug())
            });
      }
    }
  }
}
