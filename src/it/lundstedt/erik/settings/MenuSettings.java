package it.lundstedt.erik.settings;

public class MenuSettings {
  boolean showDebug = false;
  String[] menuItems = new String[] {"", "", "", ""};

  public boolean isShowDebug() {
    return showDebug;
  }

  public void setShowDebug(boolean showDebug) {
    this.showDebug = showDebug;
  }

  public String[] getMenuItems() {
    return menuItems;
  }

  public void setMenuItems(String[] menuItems) {
    this.menuItems = menuItems;
  }
}
