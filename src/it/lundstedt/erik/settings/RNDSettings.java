package it.lundstedt.erik.settings;

public class RNDSettings extends Settings {
  int high = 10, low = 0, correct = 73, err = 404;
  int level = 0;
  int max = 10;

  public RNDSettings(int high, int low, int correct, int err, int level, int max) {
    this.high = high;
    this.low = low;
    this.correct = correct;
    this.err = err;
    this.level = level;
    this.max = max;
  }

  public RNDSettings(int level, int max) {
    this.level = level;
    this.max = max;
  }

  public RNDSettings() {}

  public int getLevel() {
    return level;
  }

  public void setLevel(int level) {
    this.level = level;
  }

  public int getMax() {
    return max;
  }

  public void setMax(int max) {
    this.max = max;
  }

  public int getErr() {
    return err;
  }

  public void setErr(int err) {
    this.err = err;
  }

  public int getHigh() {
    return high;
  }

  public void setHigh(int high) {
    this.high = high;
  }

  public int getLow() {
    return low;
  }

  public void setLow(int low) {
    this.low = low;
  }

  public int getCorrect() {
    return correct;
  }

  public void setCorrect(int correct) {
    this.correct = correct;
  }
}
